## Build image
`docker build -t {tag} .`

## Supprimer tout
``
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
``

## Docker reminder
* `-p hostPort:dockerPort`

## GraphQl
* `docker build -t graph_ql .`
* Lancement du docker et accés
* `docker run -it -d -p 5454:4000 graph_ql`
* `{
  cards(name:"thalia") {id
  name}
}`

## Web app
* `docker build -t web_app .`
* `docker run --rm -it -d -p 8100:8100 -p 35729:35729 -v "/$(pwd)/src:/src/app/magic-search/src" -v "/$(pwd)/package.json:/package.json" -e "CHOKIDAR_USEPOLLING=true" web_app`

## Bdd postgrsql
* `mkdir -p $HOME/docker/volumes/postgres`
* `docker run --rm --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres`
* `psql -h localhost -U postgres -d postgres` -host -user -dbname

### Add some data

``
CREATE TABLE formats (
 name varchar(80),
 nb_cards integer
);
INSERT INTO formats VALUES ('Modern', 60);
INSERT INTO formats VALUES ('Duel Commander', 100);
``
## Adminer SGBD
* `docker run --link pg-docker:db -p 8080:8080 adminer`
* serveur : db, utilisateur : postgres, mdp : docker, bdd : postgres